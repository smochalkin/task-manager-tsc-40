package ru.smochalkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.command.AbstractSystemCommand;

import java.util.Collection;
import java.util.List;

public interface ICommandService {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractSystemCommand> getArguments();

    @NotNull
    List<String> getCommandNames();

    @NotNull
    List<String> getCommandArgs();

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @Nullable
    AbstractCommand getCommandByArg(@NotNull String arg);

    void add(@NotNull AbstractCommand command);

}
