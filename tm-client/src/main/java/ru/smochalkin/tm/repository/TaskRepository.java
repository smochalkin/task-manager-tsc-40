package ru.smochalkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository  extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public void bindTaskById(@NotNull final String projectId, @NotNull final String taskId) {
        @NotNull final Task task = findById(taskId);
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskById(@NotNull final String taskId) {
        @NotNull final Task task = findById(taskId);
        task.setProjectId(null);
    }

    @Override
    @NotNull
    public List<Task> findTasksByProjectId(@NotNull final String projectId) {
        return list.stream()
                .filter(e -> projectId.equals(e.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public void removeTasksByProjectId(@NotNull final String projectId) {
        List<Task> projectTaskList = findTasksByProjectId(projectId);
        list.removeAll(projectTaskList);
    }

}