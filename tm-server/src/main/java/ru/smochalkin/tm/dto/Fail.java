package ru.smochalkin.tm.dto;

import org.jetbrains.annotations.NotNull;

public final class Fail extends Result {

    public Fail() {
        success = false;
        message = "";
    }

    public Fail(final @NotNull Exception e) {
        success = false;
        message = e.getMessage();
    }

}
