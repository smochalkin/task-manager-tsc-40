package ru.smochalkin.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.endpoint.IProjectEndpoint;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.dto.Fail;
import ru.smochalkin.tm.dto.Result;
import ru.smochalkin.tm.dto.Success;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result createProject(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().create(session.getUserId(), name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeProjectStatusById(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().updateStatusById(session.getUserId(), id, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeProjectStatusByIndex(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().updateStatusByIndex(session.getUserId(), index, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result changeProjectStatusByName(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "status") @NotNull final String status
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().updateStatusByName(session.getUserId(), name, status);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<Project> findProjectAllSorted(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "sort") @NotNull final String strSort
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId(), strSort);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<Project> findProjectAll(
            @WebParam(name = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Project findProjectById(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Project findProjectByName(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Project findProjectByIndex(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeProjectById(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "id") @NotNull final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().removeById(session.getUserId(), id);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeProjectByName(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "name") @NotNull final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().removeByName(session.getUserId(), name);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result removeProjectByIndex(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "index") @NotNull final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().removeByIndex(session.getUserId(), index);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result clearProjects(
            @WebParam(name = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().clear(session.getUserId());
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateProjectById(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "id") @NotNull final String id,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().updateById(session.getUserId(), id, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public Result updateProjectByIndex(
            @WebParam(name = "session") @NotNull final Session session,
            @WebParam(name = "index") @NotNull final Integer index,
            @WebParam(name = "name") @NotNull final String name,
            @WebParam(name = "description") @NotNull final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getProjectService().updateByIndex(session.getUserId(), index, name, description);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}