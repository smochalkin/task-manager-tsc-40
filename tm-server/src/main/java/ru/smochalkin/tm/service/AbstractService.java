package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IRepository;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
