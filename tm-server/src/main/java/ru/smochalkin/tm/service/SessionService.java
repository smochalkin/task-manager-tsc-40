package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.api.repository.ISessionRepository;
import ru.smochalkin.tm.api.repository.IUserRepository;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.api.service.IPropertyService;
import ru.smochalkin.tm.api.service.ISessionService;
import ru.smochalkin.tm.api.service.ServiceLocator;
import ru.smochalkin.tm.enumerated.Role;
import ru.smochalkin.tm.exception.empty.EmptyIdException;
import ru.smochalkin.tm.exception.empty.EmptyLoginException;
import ru.smochalkin.tm.exception.empty.EmptyPasswordException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.exception.system.UserIsLocked;
import ru.smochalkin.tm.model.Session;
import ru.smochalkin.tm.model.User;
import ru.smochalkin.tm.util.HashUtil;

import java.sql.Connection;
import java.util.List;
import java.util.UUID;

import static ru.smochalkin.tm.util.ValidateUtil.isEmpty;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionService(
            @NotNull IConnectionService connectionService,
            @NotNull ServiceLocator serviceLocator
    ) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    @Override
    @SneakyThrows
    public void clear() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try {
                repository.clear();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Session> findAll() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            return repository.findAll();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public Session findById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            return repository.findById(id);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try {
                repository.removeById(id);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public int getCount() {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            return repository.getCount();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public final Session open(@NotNull final String login, @NotNull final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        if (user.isLock()) throw new UserIsLocked();
        @NotNull final String secret = serviceLocator.getPropertyService().getPasswordSecret();
        @NotNull final Integer iteration = serviceLocator.getPropertyService().getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(password, secret, iteration);
        if (!user.getPasswordHash().equals(hash)) throw new AccessDeniedException();
        @NotNull final Session session = new Session(user.getId());
        sign(session);
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try {
                repository.add(
                        UUID.randomUUID().toString(),
                        user.getId(),
                        session.getSignature(),
                        session.getTimestamp()
                );
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
        return session;
    }

    @Override
    @SneakyThrows
    public final void close(@NotNull final Session session) {
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try {
                repository.removeById(session.getId());
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public final void closeAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try {
                repository.removeByUserId(userId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public final void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionTarget = sign(temp);
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            try{
                repository.findById(session.getId());
            }catch (Exception e){
                throw new AccessDeniedException();
            }
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        final @NotNull User user = serviceLocator.getUserService().findById(session.getUserId());
        if (!user.getRole().equals(role)) throw new AccessDeniedException();
    }

    @NotNull
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final IPropertyService ps = serviceLocator.getPropertyService();
        @Nullable final String signature = HashUtil.salt(session, ps.getSignSecret(), ps.getSignIteration());
        session.setSignature(signature);
        return session;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Session> findAllByUserId(@Nullable final String userId) {
        if (isEmpty(userId)) throw new EmptyIdException();
        try (@NotNull final SqlSession connection = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = connection.getMapper(ISessionRepository.class);
            return repository.findAllByUserId(userId);
        }
    }

}