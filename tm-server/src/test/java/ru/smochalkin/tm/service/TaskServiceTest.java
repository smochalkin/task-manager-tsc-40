package ru.smochalkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.service.*;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.Task;

import java.util.List;
import java.util.UUID;

public class TaskServiceTest {

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectTaskService projectTaskService;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String PROJECT_ID_1 = UUID.randomUUID().toString();

    private static final String PROJECT_ID_2 = UUID.randomUUID().toString();

    private int taskCount;

    @Before
    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        projectTaskService = new ProjectTaskService(connectionService);
        taskService = new TaskService(connectionService);
        taskService.create(USER_ID_1, "test1", "test1");
        taskService.create(USER_ID_2, "test2", "test2");
        taskCount = taskService.getCount();
    }

    @After
    public void end() {
        taskService.removeByName(USER_ID_1,"test1");
        taskService.removeByName(USER_ID_2,"test2");
        taskService.removeByName(USER_ID_1,"test3");
        taskService.removeByName(USER_ID_1,"new name");
    }

    @Test
    public void createTest() {
        taskService.create(USER_ID_1, "test3", "test3");
        Assert.assertEquals(taskCount + 1, taskService.getCount());
    }


    @Test
    public void findAllTest() {
        @NotNull List<Task> taskList = taskService.findAll(USER_ID_1);
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        Assert.assertEquals(taskId, taskService.findById(USER_ID_1, taskId).getId());
    }

    @Test
    public void findByNameTest() {
        Assert.assertEquals("test1", taskService.findByName(USER_ID_1,"test1").getName());
    }

    @Test
    public void findByIndexTest() {
        Assert.assertEquals("test1", taskService.findByIndex(USER_ID_1,0).getName());
    }

    @Test
    public void removeByIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        taskService.removeById(USER_ID_1, taskId);
        Assert.assertNull(taskService.findById(USER_ID_1, taskId));
    }

    @Test
    public void removeByNameTest() {
        taskService.removeByName(USER_ID_1, "test1");
        Assert.assertNull(taskService.findByName(USER_ID_1, "test1"));
    }

    @Test
    public void removeByIndexTest() {
        taskService.removeByIndex(USER_ID_1, 0);
        Assert.assertNull(taskService.findByIndex(USER_ID_1, 0));
    }

    @Test
    public void updateByIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        taskService.updateById(USER_ID_1, taskId, "new name", "new desc");
        @NotNull final Task task = taskService.findById(USER_ID_1, taskId);
        Assert.assertEquals("new name", task.getName());
        Assert.assertEquals("new desc", task.getDescription());
    }

    @Test
    public void updateByIndexTest() {
        taskService.updateByIndex(USER_ID_1, 0, "new name", "new desc");
        @NotNull final Task task = taskService.findByIndex(USER_ID_1, 0);
        Assert.assertEquals("new name", task.getName());
        Assert.assertEquals("new desc", task.getDescription());
    }

    @Test
    public void updateStatusByIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        taskService.updateStatusById(USER_ID_1, taskId, "COMPLETED");
        @NotNull final Task task = taskService.findById(USER_ID_1, taskId);
        Assert.assertEquals(Status.COMPLETED,task.getStatus());
    }

    @Test
    public void updateStatusByNameTest() {
        taskService.updateStatusByName(USER_ID_1, "test1", "COMPLETED");
        @NotNull final Task task = taskService.findByName(USER_ID_1, "test1");
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void updateStatusByIndexTest() {
        taskService.updateStatusByIndex(USER_ID_1, 0, "COMPLETED");
        @NotNull final Task task = taskService.findByIndex(USER_ID_1, 0);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void isNotIndex() {
        Assert.assertTrue(taskService.isNotIndex(USER_ID_1, 99));
        Assert.assertFalse(taskService.isNotIndex(USER_ID_1, 0));
    }

    @Test
    public void bindTaskByProjectIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        projectTaskService.bindTaskByProjectId(USER_ID_1, PROJECT_ID_1, taskId);
        @NotNull final Task task = taskService.findById(USER_ID_1, taskId);
        Assert.assertEquals(PROJECT_ID_1, task.getProjectId());
    }

    @Test
    public void unbindTaskByProjectIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        projectTaskService.bindTaskByProjectId(USER_ID_1, PROJECT_ID_1, taskId);
        Assert.assertEquals(PROJECT_ID_1, taskService.findById(USER_ID_1, taskId).getProjectId());
        projectTaskService.unbindTaskByProjectId(USER_ID_1, PROJECT_ID_1, taskId);
        Assert.assertNotEquals(PROJECT_ID_1, taskService.findById(USER_ID_1, taskId).getProjectId());

    }

    @Test
    public void findTasksByProjectIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        projectTaskService.bindTaskByProjectId(USER_ID_1, PROJECT_ID_1, taskId);
        Assert.assertEquals(1, projectTaskService.findTasksByProjectId(USER_ID_1, PROJECT_ID_1).size());
    }

    @Test
    public void removeProjectByIdTest() {
        @NotNull final String taskId = taskService.findByName(USER_ID_1,"test1").getId();
        projectTaskService.bindTaskByProjectId(USER_ID_1, PROJECT_ID_1, taskId);
        Assert.assertEquals(1, projectTaskService.findTasksByProjectId(USER_ID_1, PROJECT_ID_1).size());
        projectTaskService.removeProjectById(PROJECT_ID_1);
        Assert.assertEquals(0, projectTaskService.findTasksByProjectId(USER_ID_1, PROJECT_ID_1).size());
    }

}
